#!/usr/bin/env python3

from mock_python_slow_ctl import SlowControl

execute = SlowControl()
execute.command_file = "mock_ring_check_with_delay.txt"
execute.execute_file(execute.command_file)
execute.command_file = "mock_ring_check.txt"
execute.execute_file(execute.command_file)
