# This should be a test startup script
require execute
require epicsexecute

iocshLoad("$(execute_DIR)/execute.iocsh")

executeAddCommand("CMD0", "mock_ring_check.py", 0)
dbLoadRecords("$(execute_DIR)db/execute.db", "P=Test:,R=Execute:,CMD=CMD0")

iocInit()
