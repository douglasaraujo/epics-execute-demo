from time import sleep
import random
from typing import List

class SlowControl:
    def __init__(self) -> None:
        self._command_file: str = ""

    def open_file(self, fname, wr = ''):
        try:
            fp = open(fname, wr)
        except IOError:
            print(f"Unable to open file: {fname}")   
        return fp

    @property
    def command_file(self):
        return self._command_file

    @command_file.setter
    def command_file(self, val):
        self._command_file = val

    def execute_file(self, file_name):
        file = self.open_file(file_name, "r")

        for line in file:
            word_arr: List[str] = line.split()

            # ignore commented or blank lines (with support for windows line endings)
            if ("#" == word_arr[0][0]) or ('\n' == word_arr[0][0]) or ('\r' == word_arr[0][0]):
                continue

            cmd:str = word_arr[1]

            if cmd == "MSG":
                print(f"{line.rstrip()}")

            elif cmd == "WAIT":
                time_s = int(word_arr[2]) 
                print(f"Waiting {time_s} seconds")
                sleep(time_s)
                print("Time over")

            elif cmd == "AGET":
                line_num  = word_arr[0]
                addr_name = word_arr[3]
                exp_value = word_arr[4]

                # generate random data to test
                n = random.randint(0,1)
                data = format(n, "#010x")

                if data.upper() != exp_value.upper():
                    print(f"\n!!! MISMATCH on line {line_num}")
                    print(f"Read {data} from address {addr_name}, expected {exp_value}")
